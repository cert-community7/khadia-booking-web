import React, { Suspense } from 'react';
import {render} from 'react-dom';
import { Provider } from 'mobx-react';
import allStores from 'stores';
import RouteWrapper from 'components/RouterWrapper';
import 'antd/dist/antd.css';
import "./index.css";

render(
  <Suspense fallback={<div>Loading.....</div>}>
  <Provider {...allStores}>
   <RouteWrapper />
  </Provider>
  </Suspense>,
  document.getElementById('root')
);
