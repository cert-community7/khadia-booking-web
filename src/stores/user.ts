import { API_URI, COOKIE_KEYS } from 'config/config'
import { observable , action, makeObservable } from 'mobx'
import axiosWrapper from 'utils/axiosWrapper'
import { wrappedResponse } from 'utils/commons'
import { setCookieItem } from 'utils/cookieStorage'

type User = {
  id? : string
}

type GetLoginRequest = {
  email: string,
  password: string
}

export default class UserStore {
  @observable loginLoader: Boolean = false
  @observable user: User = {}

  constructor(){
    makeObservable(this);
  }

  @action
  setUser = (user: User) => {
    this.user = user
  }

  @action
  setLoginLoader = (state: Boolean) => {
    this.loginLoader = state
  }

  getLoggedInUserRequestParameters = (userData: any) : GetLoginRequest => {
    return {
      email: userData.email,
      password: userData.password
    }
  }

  @action
  getLoggedInUser = async (userData) => {
    try{
      this.setLoginLoader(true);
      //await generateDelay(5000)
      const dataToSend = this.getLoggedInUserRequestParameters(userData)
      const {response} = await axiosWrapper.post(API_URI.LOGIN_URL ,dataToSend);
      const serverData = response.data
      this.setUserToken(serverData.token)
      return wrappedResponse({data: serverData, result: true});
    }catch(error){
      console.log(error , 52)
      return wrappedResponse({data: undefined, result: false})
    }finally{
      this.setLoginLoader(false);
    }
  }  

  setUserToken = (token) => {
    setCookieItem(COOKIE_KEYS.USER_TOKEN , token);
  }

}

