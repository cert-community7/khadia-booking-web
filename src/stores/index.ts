import UserStore from "./user";

const initStores = () => {
    return {
        userStore: new UserStore()
    }
}

const allStores = initStores();

export default allStores;
