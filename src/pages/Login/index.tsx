import React from 'react';
import { Form, Input, Button, Row, Col } from 'antd';
import css from './Login.css';
import { inject, observer } from 'mobx-react';

const LoginPage = (props) => {
  const { loginLoader } = props.userStore;

  const onFormFinish = async (values) => {
    const { getLoggedInUser } = props.userStore;
    console.log(await getLoggedInUser(values));
  };

  const onFormFailed = () => {};

  return (
    <div className={css.pageContainer}>
      <section className={css.loginContainer}>
        <Row>
          <Col span={24}>
            <header className={css.loginTitle}>Login</header>
          </Col>
        </Row>
        <main className={css.loginForm}>
          <Form
            name="loginForm"
            onFinish={onFormFinish}
            onFinishFailed={onFormFailed}
          >
            <Form.Item
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 12 }}
              className={css.formFields}
              label="Email"
              name="email"
              rules={[
                { required: true, message: 'Please input your username!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 12 }}
              className={css.formFields}
              label="Password"
              name="password"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Row>
              <Col
                xs={{ offset: 0, span: 24 }}
                sm={{ offset: 8, span: 12 }}
                md={{ offset: 8, span: 12 }}
              >
                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className={css.submitButton}
                    loading={loginLoader}
                  >
                    Submit
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </main>
      </section>
    </div>
  );
};

export default inject('userStore')(observer(LoginPage));
