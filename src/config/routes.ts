// eslint-ignore
import React from "react";

export const routes = {
    "SampleOne" : {
        path: "/",
        exact: true,
        generateRoute: () => {
            return '/'
        },
        component: React.lazy(() => import('pages/Login'))
    },
    "SampleTwo" : {
        path: "/two",
        exact: true,
        generateRoute: () => {
            return '/SampleTwo'
        },
        component: React.lazy(() => import('pages/SampleTwo'))
    }
}

export default Object.entries(routes);