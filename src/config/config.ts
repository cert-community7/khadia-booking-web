export const isDevelopmentMode = () => {
    return process.env.REACT_APP_ENVIRONMENT === 'develop'
}

export const CONSTANTS = {
    API_TIMEOUT: 10000,
    DEFAULT_COOKIE_DAYS: 1,
}

export const ENV = {
  BASE_SERVER_URI:  process.env.REACT_APP_BASE_SERVER_URI,
  COOKIE_DAYS: Number(process.env.REACT_APP_COOKIE_DAYS) || CONSTANTS.DEFAULT_COOKIE_DAYS
} 

export const API_URI = {
    LOGIN_URL: '/users/login'
}

export const COOKIE_KEYS = {
    USER_TOKEN: 'user_token'
}