import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { CONSTANTS, COOKIE_KEYS, ENV } from 'config/config';
import { getCookieItem } from './cookieStorage';

type AxiosWrapperInit = {
  baseUrl: string;
  timeout: number;
};

type ApiCallResponse = Promise<{
    response: any,
    axios: AxiosResponse
}>



class AxiosWrapper {
  apiCaller: AxiosInstance;

  constructor({ baseUrl, timeout }: AxiosWrapperInit) {
    this.apiCaller = axios.create({
      baseURL: baseUrl,
      timeout,
    });
  }

  get headers() {
    const cookie = getCookieItem(COOKIE_KEYS.USER_TOKEN);
    return {
      'Content-Type': 'application/json',
      ...(cookie ? {"authorization" : cookie} : {} )
    };
  }

  validateStatus = (status) => {
    return status >= 200 && status <= 299;
  };

  transformResponse = (response) => {
    console.log(response , 37)
      return {response: response.data, axios: response};
  }

  get = (url: string, params?: any, extraConfigs?: AxiosRequestConfig) : ApiCallResponse => {
    return this.apiCaller.get(url, {
      headers: this.headers,
      params,
      validateStatus: this.validateStatus,
      ...extraConfigs,
    }).then(this.transformResponse);
  };

  post = (url: string, data?: any) : ApiCallResponse => {
    return this.apiCaller.post(url, data, {
      headers: this.headers,
      validateStatus: this.validateStatus,
    }).then(this.transformResponse);
  };

  put = (url: string, data?: any) : ApiCallResponse => {
    return this.apiCaller.put(url, data, {
      headers: this.headers,
      validateStatus: this.validateStatus,
    }).then(this.transformResponse);
  };

  // prefixed with underscored because delete is a reserved word in javascript
  delete = (url: string) : ApiCallResponse => {
    return this.apiCaller.delete(url, {
      headers: this.headers,
      validateStatus: this.validateStatus,
    }).then(this.transformResponse);
  };
}

export default new AxiosWrapper({
    baseUrl: ENV.BASE_SERVER_URI,
    timeout: CONSTANTS.API_TIMEOUT
})