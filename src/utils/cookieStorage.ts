import { ENV } from 'config/config';
import { CookieStorage } from 'cookie-storage';
import { CookieOptions } from 'cookie-storage/lib/cookie-options';

const getCookieExpireTime = () => {
  let currentDate = new Date();
  currentDate.setDate(currentDate.getDate() + ENV.COOKIE_DAYS);
  return currentDate;
};

export const cookieStorage = new CookieStorage({
  sameSite: 'Strict',
  path: '/',
});

export const setCookieItem = (
  key: string,
  value,
  extraConfig: CookieOptions = {}
) => {
  value = JSON.stringify(value);
  cookieStorage.setItem(key, value, {
    ...extraConfig,
    expires: getCookieExpireTime(),
  });
};

export const getCookieItem = (key: string) => {
  const value = cookieStorage.getItem(key);
  return value ? JSON.parse(value) : value;
};

export const clearCookieStorage = () => {
  cookieStorage.clear();
};
