type WrappedResponse = {
    data: any,
    result: Boolean
}

export const wrappedResponse = ({data , result}: WrappedResponse) : WrappedResponse => {
    return {
        data,
        result
    }
}

export const generateDelay = (time) => {
    return new Promise((res) => {
        setTimeout(res , Math.max(Number(time) || 0 , 1000))
    })
}