import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import routes from "config/routes";


class RouteWrapper extends React.Component {

    render(){
       return <Router>
       <Switch>
           {routes.map(([key , route]) => {
            const { component , exact , path } = route
            return <Route 
            path={path}
            key={key} 
            component={component} 
            exact={exact}> 
            </Route>
           })}
       </Switch>
       </Router>
    }
}

export default RouteWrapper;